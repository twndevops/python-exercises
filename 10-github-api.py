from requests import get

# Write a program that:
    # connects to GitHub API
    # gets all public repos for a GitHub user (yourself)
    # prints each repo's name and URL

# https://docs.github.com/en/rest/repos/repos?apiVersion=2022-11-28#list-repositories-for-a-user
pub_repos = get("https://api.github.com/users/unatarajan/repos")
pub_repos_list = pub_repos.json()
for repo in pub_repos_list:
    print(f"See repo {repo["name"]} here: {repo.get("url")}.")

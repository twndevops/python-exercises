from datetime import datetime
from math import floor

# Write a program that:
    # accepts the user's birthday as input
    # calculates and prints days, hours and minutes remaining until their next birthday


def calc_hrs_mins(delta):
    hours_left = floor(delta.seconds / 3600)
    mins_left = floor(((delta.seconds / 3600) - hours_left) * 60)
    return f"{delta.days} days, {hours_left} hrs, and {mins_left} mins remaining!"


def time_until_birthday():
    birthday = input("Enter your birthday (mm/dd) to return the number of days until the next celebration!\n")

    curr_year = datetime.today().year

    birth_date_curr_year = datetime.strptime(f"{birthday}/{curr_year}", "%m/%d/%Y")
    print(birth_date_curr_year)

    delta_curr_year = birth_date_curr_year - datetime.today()
    print(delta_curr_year)

    # if birthday using curr year is in the future, return days/hrs/mins remaining
    if delta_curr_year.days >= 0:
        return calc_hrs_mins(delta_curr_year)
    # if birthday using curr year is in the past, update to next year
    else:
        birth_date_next_year = datetime.strptime(f"{birthday}/{curr_year + 1}", "%m/%d/%Y")
        print(birth_date_next_year)

        delta_next_year = birth_date_next_year - datetime.today()
        print(delta_next_year)

        return calc_hrs_mins(delta_next_year)


print(time_until_birthday())

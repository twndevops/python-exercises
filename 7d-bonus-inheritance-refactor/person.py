class Person:
    def __init__(self, full_name, age, occupation):
        self.full_name = full_name
        self.age = age
        self.occupation = occupation
        # ONLY lecture names
        self.lectures = []

    def print_full_name(self):
        print(f"{self.occupation}'s name is {self.full_name}.")

    def print_lectures(self):
        if len(self.lectures) == 0:
            print(f"{self.occupation} {self.full_name} attends no lectures.")
        for index, lecture in enumerate(self.lectures):
            print(f"Lecture {index + 1}: {lecture}")

from univdb import all_lectures
from person import Person


class Professor(Person):
    def add_lecture(self, new_subject_name):
        # find new_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == new_subject_name:
                # add professor's name to lecture's professors list
                lecture.professors.append(self.full_name)
                # add lecture name to professor's lectures list
                self.lectures.append(lecture.name)

    def remove_lecture(self, removed_subject_name):
        # find removed_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == removed_subject_name:
                # remove professor's name from lecture's professors list
                lecture.professors.remove(self.full_name)
                # remove lecture name from professor's lectures list
                self.lectures.remove(lecture.name)

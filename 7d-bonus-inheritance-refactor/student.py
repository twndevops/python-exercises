from univdb import all_lectures
from person import Person


class Student(Person):
    def add_lecture(self, new_subject_name):
        # find new_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == new_subject_name:
                # add lecture name to student's lectures list
                self.lectures.append(lecture.name)

    def remove_lecture(self, removed_subject_name):
        # find removed_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == removed_subject_name:
                # remove lecture name from student's lectures list
                self.lectures.remove(lecture.name)

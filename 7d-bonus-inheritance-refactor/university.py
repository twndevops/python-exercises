from univdb import all_lectures
from lecture import Lecture
from professor import Professor
from student import Student

# --- STEP 1: Create lectures / subjects w/o professors in DB list all_lectures ---


def create_lecture(name, max_students, duration):
    new_lecture = Lecture(name, max_students, duration)
    all_lectures.append(new_lecture)
    # following line was to test print_name_duration method
    # return new_lecture


create_lecture("acting", 10, 3)
create_lecture("writing", 15, 1.5)
create_lecture("improv", 12, 2)
create_lecture("potions", 11, 2.75)
create_lecture("charms", 14, 1.25)
# charms = create_lecture("charms", 14, 1.25)
# charms.print_name_duration()


# --- STEP 2: Create professors, add/ remove subjects ---

emma = Professor("Emma Thompson", 60, "Professor")
albus = Professor("Albus Dumbledore", 127, "Professor")

emma.print_full_name()
emma.print_lectures()
emma.add_lecture("acting")
emma.add_lecture("writing")
emma.add_lecture("improv")
emma.print_lectures()
emma.remove_lecture("writing")
emma.print_lectures()

albus.print_full_name()
albus.print_lectures()
albus.add_lecture("charms")
albus.add_lecture("potions")
albus.print_lectures()
albus.remove_lecture("charms")
albus.print_lectures()

for lecture in all_lectures:
    print(f"{lecture.name} lecture is taught by:")
    for professor in lecture.professors:
        print(f"Professor {professor}")

# --- STEP 3: Create students, add/ remove lectures ---

tim = Student("Tim Chalamet", 29, "Student")
tina = Student("Tina Turner", 43, "Student")

tim.print_full_name()
tim.print_lectures()
tim.add_lecture("acting")
tim.add_lecture("writing")
tim.add_lecture("improv")
tim.print_lectures()
tim.remove_lecture("writing")
tim.print_lectures()

tina.print_full_name()
tina.print_lectures()
tina.add_lecture("charms")
tina.add_lecture("potions")
tina.print_lectures()
tina.remove_lecture("charms")
tina.print_lectures()

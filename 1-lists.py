my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]

# Print out all the elements of the list that are higher than or equal 10.

for num in my_list:
    if num >= 10:
        print(num)

# Instead of printing the elements one by one, make and print a new list.

my_filtered_list = []

for num in my_list:
    if num >= 10:
        my_filtered_list.append(num)

print(my_filtered_list)

# Ask the user to input a number. Print a list that contains only elements that are higher than the input number.


def higher_than_lower_bound(low):
    try:
        whole_low = int(low)
        user_filtered_list = []
        for num in my_list:
            if num > whole_low:
                user_filtered_list.append(num)
        print(user_filtered_list)
    except ValueError:
        print(f"Your input {low} is not a whole number. Try again.")

lower_bound = ""

while(lower_bound != "q"):
    lower_bound = input("Enter a whole number to return all my_list nums higher than your number. Enter 'q' to quit.\n")
    if lower_bound == "q":
        continue
    higher_than_lower_bound(lower_bound)
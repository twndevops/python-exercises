from math import inf

employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}

employee["job"] = "Software Engineer"

del employee["age"]
# can also pop off an attr
# age = employee.pop("age")
# print(age)

for attr in employee:
    print(f"{attr}:{employee.get(attr)}")

# ---

dict_one = {'a': 100, 'b': 400}
dict_two = {'x': 300, 'y': 200}

# Py equivalent of JS spread syntax: **
combo_dict = {**dict_one, **dict_two}
print(combo_dict)
# can copy and update
# combo_dict = dict_one.copy()
# combo_dict.update(dict_two)
# print(combo_dict)

sum = 0
# many ways to represent inf, used built-in math module
max = -inf
min = inf

# Reference: https://realpython.com/iterate-through-dictionary-python/#looping-over-dictionary-items-the-items-method
# can only extract val as well if dict is converted to list of key:val tuples [('a', 100), ...]
for key, val in combo_dict.items():
    sum += val
    if val < min:
        min = val
    if val > max:
        max = val

print(f"Sum: {sum}, Max: {max}, Min: {min}")

# Alternative approach: convert dict values to list, sort() w/o args, print first and last vals in sorted list

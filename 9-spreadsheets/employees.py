from pandas import *

data_frame = read_excel("9-spreadsheets/employees.xlsx")

# delete last 2 cols
two_col_data_frame = data_frame.drop(columns=['Job Title', 'Date of Birth'])

# sort by 'Years of Experience', desc
sorted_two_col_data_frame = two_col_data_frame.sort_values(by=['Years of Experience'], ascending=False)

# write changes to new .xlsx file in same dir
sorted_two_col_data_frame.to_excel("9-spreadsheets/employees_sorted.xlsx", index=False)

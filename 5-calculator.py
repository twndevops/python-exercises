# Write a simple calculator program that:
    # takes 3 user inputs: 2 numbers, operation to execute
    # handles plus, minus, multiply, divide operations
    # validates inputs (only nums allowed) and gives feedback
    # runs until the user types “exit”
    # prints # of calculations the user has done when they exit

calc_inputs = ""
calc_count = 0

while(calc_inputs != "exit"):
    calc_inputs = input("Enter 2 nums and an operation to do on those nums in order provided (+, -, *, /), comma-separated. Enter 'exit' to quit.\n")

    if calc_inputs == 'exit':
        print(f"{calc_count} calculations done successfully this session.")
        continue

    calc_inputs_list = calc_inputs.split(",")
    if len(calc_inputs_list) != 3:
        print(f"Provide no more or less than 3 inputs.")
        continue

    try:
        # following throw ValueErrors if passed strings that don't evaluate to ints or floats
        operand1 = float(calc_inputs_list[0])
        operand2 = float(calc_inputs_list[1])

        # Reference: https://www.freecodecamp.org/news/python-switch-statement-switch-case-example/
        match calc_inputs_list[2]:
            case "+":
                print(operand1 + operand2)
            case "-":
                print(operand1 - operand2)
            case "*":
                print(operand1 * operand2)
            case "/":
                print(operand1 / operand2)
            case _:
                print("Provide a valid operator.")
                continue

        calc_count += 1

    except ValueError:
        print(f"Provide 2 nums.")

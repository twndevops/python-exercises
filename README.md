#### Notes for "Module 12 - Programming with Python" exercises

EXERCISES 1 and 4:
- NOTE: `filter` method changes data type from `list` to `filter` so cannot directly print a filtered list.
```
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]

def filter_list(nums):
     for num in nums:
         if num >= 10:
             return True

 my_filtered_list = filter(filter_list, my_list)
 print(type(my_filtered_list)) # <class 'filter'>
```

EXERCISE 2:
- "Write a Python script" means run a `.py` file like so: `python [filename].py`.

EXERCISE 6:
- Used [built-in `random` module](https://docs.python.org/3.3/library/random.html) to generate random numbers.

EXERCISE 7: Write a program to handle data of university students, professors, and lectures/ subjects using classes.

- `univdb.py` is the master list of all lectures/ subjects available in university.
- `university.py` is the main Py script testing all classes and their methods.
- Only Professor and Student objs can add or remove themselves from a lecture/ subject.

- a) Create a `Student` class 
    - properties: first name, last name, age, and lectures (s)he attends
        - NOTE: Changed from first/last to `full_name`, to be consistent w/ `Professor` class.
    - methods: print full name,  list attending lectures, attend a new lecture, leave a lecture

- b) Create a `Professor` class
    - properties: first name, last name, age, and subjects (s)he teaches
        - NOTE: Changed from first/last to `full_name`, to make it easier to add the professor's full name to the master list of lecture objs in `univdb.py`.
    - methods: print full name, list subjects taught, add new subject, remove subject

- c) Create a `Lecture` class
    - properties: name, max number of students, duration, list of professors teaching the lecture
    - methods: print name and duration of the lecture
        - NOTE: Didn't include a method to add a professor to the list of professors teaching the lecture.
            - ALL LECTURE OBJ UPDATES ARE MADE FROM `Professor` CLASS METHODS!

- d) Bonus - Refactor `Student` and `Professor` classes to inherit the following from `Person` class:
  - See 7d dir for refactored files
  - properties: first name, last name, age
    - NOTES:
      - Changed from first/last to `full_name`, to make it easier to add the professor's full name to the master list of lecture objs in `univdb.py`.
      - Added `occupation`.
  - methods: print name
    - NOTES:
      - Moved `print_full_name` and `print_lectures` methods up to Person, using `occupation` to distinguish `Student` and `Professor` objs.
      - Methods remaining in `Student` and `Professor` classes differ only in that a `Professor` must update the master list `all_lectures` in `univdb.py`.
    
- Soln code is problematic for many reasons:
  - All of the `Student`/`Professor` properties and half the methods can be moved up to `Person` class/ inherited. Soln classes are still too redundant.
  - Removing a lecture as a `Student`/`Professor` involves popping a lecture dict off of a list of lecture dicts.
    - Popping off a list by default removes the last item. Can pass a list index to pop off an item at a specific index. However, she's popping a list dict off?
  - A `Lecture` obj can add a `Professor`, which makes no sense. When a `Professor` adds multiple `Lecture`s, each `Lecture` must be updated to add that `Professor`.
  - When a `Student`/`Professor` is created, multiple `Lecture`s can be added as a list to `Student`/`Professor` obj. In your model, a `Student`/`Professor` is created and then can add/remove `Lecture`s one-at-a-time.

EXERCISE 9:
- Used `pandas` external pkg instead of `openpyxl` to complete tasks.
  - Per [this openpyxl doc](https://openpyxl.readthedocs.io/en/latest/filters.html), can add relevant instructions to filter/ sort to the file but these will neither actually filter nor sort.
  - List of `pandas` docs referenced:
    - [read_excel](https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html)
    - [DataFrame.drop](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.drop.html)
    - [DataFrame.sort_values](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_values.html)
    - [DataFrame.to_excel](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_excel.html)
- Soln used `openpyxl`:
  - Deleted last 2 cols
  - Iterated over row to create a list of dicts: `[{"name": employee_name , "experience": employee_yrs}, ...]`
  - Used `itemgetter` from Python's built-in `operator` module to sort dicts list by employee_yrs
  - Added cell values into new file sheet and saved new file

EXERCISE 10:
- Used `requests` HTTP Library to facilitate connecting Python module `10-github-api.py` to a remote web application (GitHub).

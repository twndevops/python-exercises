class Lecture:
    def __init__(self, name, max_students, duration):
        self.name = name
        self.max_students = max_students
        self.duration = duration
        # ONLY professor full names
        self.professors = []

    def print_name_duration(self):
        print(f"{self.name} lecture lasts {self.duration} hrs.")

from univdb import all_lectures


class Student:
    def __init__(self, full_name, age):
        self.full_name = full_name
        self.age = age
        # ONLY lecture names
        self.lectures = []

    def print_full_name(self):
        print(f"Student's name is {self.full_name}.")

    def print_lectures(self):
        if len(self.lectures) == 0:
            print(f"Student {self.full_name} attends no lectures.")
        for index, lecture in enumerate(self.lectures):
            print(f"Lecture {index+1}: {lecture}")

    def add_lecture(self, new_subject_name):
        # find new_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == new_subject_name:
                # add lecture name to student's lectures list
                self.lectures.append(lecture.name)

    def remove_lecture(self, removed_subject_name):
        # find removed_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == removed_subject_name:
                # remove lecture name from student's lectures list
                self.lectures.remove(lecture.name)

from univdb import all_lectures


class Professor:
    def __init__(self, full_name, age):
        self.full_name = full_name
        self.age = age
        # ONLY lecture names
        self.subjects = []

    def print_full_name(self):
        print(f"Professor's name is {self.full_name}.")

    def print_subjects(self):
        if len(self.subjects) == 0:
            print(f"Professor {self.full_name} teaches no subjects.")
        for index, subject in enumerate(self.subjects):
            print(f"Lecture {index+1}: {subject}")

    def add_subject(self, new_subject_name):
        # find new_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == new_subject_name:
                # add professor's name to lecture's professors list
                lecture.professors.append(self.full_name)
                # add lecture name to professor's subjects list
                self.subjects.append(lecture.name)

    def remove_subject(self, removed_subject_name):
        # find removed_subject in all_lectures
        for lecture in all_lectures:
            if lecture.name == removed_subject_name:
                # remove professor's name from lecture's professors list
                lecture.professors.remove(self.full_name)
                # remove lecture name from professor's subjects list
                self.subjects.remove(lecture.name)

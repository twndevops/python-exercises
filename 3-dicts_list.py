employees = [{
  "name": "Tina",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer",
  "address": {
    "city": "New York",
    "country": "USA"
  }
},
{
  "name": "Tim",
  "age": 35,
  "birthday": "1985-02-21",
  "job": "Developer",
  "address": {
    "city": "Sydney",
    "country": "Australia"
  }
}]

# Print the name, job and city of each employee using a loop.

for employee in employees:
    print(f"Employee name: {employee["name"]}, job: {employee.get("job")}, city: {employee["address"].get("city")}")

# Print the country of the second employee in the list.

print(employees[1].get("address")["country"])

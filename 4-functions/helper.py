from math import inf

# Write a function that accepts a list of employee dicts and prints out the name and age of the youngest employee.


def youngest_employee(employees_list):
    # can also initialize w/ first employee's age and obj
    min_age = inf
    youngest = {}
    for employee in employees_list:
        if employee["age"] < min_age:
            min_age = employee["age"]
            youngest = employee
    print(f"The youngest employee is {youngest.get("name")} at age {youngest["age"]}.")

# Write a function that accepts a string and calculates the number of upper case letters and lower case letters.


def num_upper_lower(string):
    num_upper = 0
    num_lower = 0
    # option to convert string to list using constructor func list()
    for char in string:
        if char.isupper():
            num_upper += 1
        elif char.islower():
            num_lower += 1
    print(f"There are {num_upper} uppercase and {num_lower} lowercase chars in given string.")

# Write a function that prints the even numbers from a provided list.


def even_nums(nums_list):
    def filter_evens(num):
        if num % 2 == 0:
            return True

    filtered_list = filter(filter_evens, nums_list)

    # cannot directly print filtered_list's contents
    for num in filtered_list:
        print(num)

from random import randint

# Write a program that:
    # generates a random number between 1 and 9 (inclusive)
    # runs until the user guesses the number
    # on each guess, prints whether they guessed too low/ high
    # on a correct guess, prints out YOU WON! and exits

random_int = randint(1,9)
print(random_int)

num_guess = 0

while num_guess != random_int:
    input_guess = input("Guess a random whole num b/w 1 and 9, inclusive.\n")

    try:
        # if input_guess is str or float, will throw ValueError
        # str representations of neg ints will convert w/o issue
        num_guess = int(input_guess)

        if num_guess == random_int:
            print("YOU WON!")
        elif num_guess < random_int:
            print("Too low!")
        else:
            print("Too high!")

    except ValueError:
        print("Provide a whole num b/w 1 and 9, inclusive.")
